import React, { useState } from 'react';
import AddTaskForm from './components/AddTaskForm.jsx';
import UpdateForm from './components/UpdateForm.jsx';
import ToDo from './components/ToDo.jsx';

import 'bootstrap/dist/css/bootstrap.min.css';


import './App.css';

function App() {
  // Task state
  const [toDo, setToDo] = useState([]);

  // Temp State
  const [newTask, setNewTask] = useState('');
  const [updateData, setUpdateData] = useState('');

  // Add Task
  const AddTask = () => {
    if (newTask) {
      let num = toDo.length + 1;
      let newEntry = { id: num, title: newTask, status: false };
      setToDo([...toDo, newEntry]);
      setNewTask('');
    }
  }

  // Delete Task
  const deleteTask = (id) => {
    let newTask = toDo.filter( task => task.id !== id)
    setToDo(newTask);
    // Implement your deleteTask logic here
  }

  // Mark Task as done
  const markDone = (id) => {
    let newTask = toDo.map( task => {
      if (task.id === id) {
        return ({...task, status: !task.status})
      }
      return task;
    })
    setToDo(newTask);
    // Implement your markDone logic here
  }

  // Cancel update
  const cancelUpdate = () => {
    setUpdateData('');
    // Implement your cancelUpdate logic here
  }

  // Change task for update
  const changeTask = (e) => {
    let newEntry = {
      id: updateData.id,
      title: e.target.value,
      status: updateData.status ? true : false
    }
    // Implement your changeTask logic here
    setUpdateData(newEntry);
  }

  // Update task
  const updateTaskchangeTask = (e) => {
    let filterRecords = [...toDo].filter( task => task.id !== updateData.id );
    let updateObject = [...filterRecords, updateData] 
    setToDo(updateObject);
    setUpdateData ('');
    // Implement your updateTaskchangeTask logic here
  }

  const newLocal = 'No Task, make a task now!';
  // Return the main JSX structure for the App component
  return (
    // The main container div with the 'container' and 'App' classes
    <div className="container App">
      {/* Add spacing with line breaks */}
      <br /><br />

      {/* Heading for the To-Do List App */}
      <h2>To Do List App (ReactJS)</h2>

      {/* Additional spacing with line breaks */}
      <br /><br />

      {updateData ? (
  <UpdateForm 
    updateData={updateData}
    changeTask={changeTask}
    updateTask={updateTaskchangeTask}  // Corrected prop
    cancelUpdate={cancelUpdate}
  />
) : (
  <AddTaskForm 
    newTask={newTask}
    setNewTask={setNewTask}
    addTask={AddTask}
  />
)}

{toDo && toDo.length ? '' : 'No Task Here...'}

      <ToDo
      toDo={toDo}
      markDone={markDone}
      setUpdateData={setUpdateData}
      deleteTask={deleteTask}
      />

    </div>
  );
}

export default App;
