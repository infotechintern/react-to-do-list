const AddTaskForm = ( { newTask, setNewTask, addTask }) => {
    return (
        <>
        {/* Add Task */}
      <div className="row">
        <div className="col">
          <input
            value={newTask}
            onChange={(e) => setNewTask(e.target.value)}
            type="text"
            className="form-control form-control-lg"
            placeholder="Enter new task"
          />
        </div>
        <div className="col-auto">
          <button
            type="button"
            className="btn btn-lg btn-success"
            onClick={addTask}
          >
            Add Task
          </button>
        </div>
      </div>
      <br />
        </>

    )
}

export default AddTaskForm;